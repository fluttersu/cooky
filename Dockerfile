# This is just a copy of the image we use uploaded by santetis.
# I just wanted to make sure we don't lose it in case they delete it.
FROM ubuntu:bionic

LABEL maintainer="kevin@santetis.fr"

# Update ubuntu
RUN apt-get update -qq
# Install needed program
RUN apt-get install -qq apt-transport-https curl gnupg lcov git nodejs

# Install dart
RUN sh -c 'curl https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -' 
RUN sh -c 'curl https://storage.googleapis.com/download.dartlang.org/linux/debian/dart_stable.list > /etc/apt/sources.list.d/dart_stable.list'
RUN apt-get update -qq
RUN apt-get install -qq dart
ENV PATH="/usr/lib/dart/bin:$PATH"
ENV PATH="/root/.pub-cache/bin:$PATH"
RUN pub global activate coverage

#   Install nodejs
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install -y nodejs
RUN npm install -g firebase-tools


# Install chrome
RUN echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google.list
RUN apt-get update -qq
RUN apt-get install -qq google-chrome-stable

# Install firefox
RUN apt-get install -qq firefox
